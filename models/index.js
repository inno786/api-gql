const mongoose = require('mongoose');

module.exports.connect = () => {
  mongoose.connect('mongodb://iftekhar:iftekhar123@ds161074.mlab.com:61074/hotel-reservation', { useNewUrlParser: true });
  mongoose.set('useCreateIndex', true);
  mongoose.connection.on('error', err => {
    console.error(`Mongoose connection error: ${err}`);
    process.exit(1);
  });

  // load models
  require('./Hotel');
  require('./BookHotel');
};
