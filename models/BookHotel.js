const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let BookHotelSchema = new Schema({
    fkHotelId: {
        type: String,
        ref: 'Hotel'
    },
    checkInDate: {
        type: String
    },
    checkOutDate: {
        type: String
    }
});

module.exports = mongoose.model('BookHotel', BookHotelSchema);